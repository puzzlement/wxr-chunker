About WXR Chunker
=================

Split large WordPress eXtended RSS (WXR) export files into manageable chunks
for uploading. Unlike many other such scripts, this one will make sure that
attachments and all posts/pages that use those attachments are still contained
in the same file.

Requirements
============

Python 3.

Usage
=====

    usage: split.py [-h] [-s SIZE] [--strict] [-z] [-v] input_wxr_filename
    
    Split WordPress WXR file into smaller files, preserving links between
    attachments and posts
    
    positional arguments:
      input_wxr_filename    Input WXR file
    
    optional arguments:
      -h, --help            show this help message and exit
      -s SIZE, --size SIZE  Maximum size of the output files in MB (default: 2)
      --strict              Use strict size measures rather than estimates for
                            gzipping, decreases performance by around 4 times
                            (default: false)
      -z, --gzip            Gzip the output files (default: false)
      -v, --verbose         Verbose output (default: false)

Author
======

WXR Chunker is by [Mary Gardiner](https://mary.gardiner.id.au/).

License
=======

WXR Chunker is available under the MIT License. See the LICENSE file for the
licence.
