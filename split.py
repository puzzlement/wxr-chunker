#!/usr/bin/env python3

from lxml import etree
import argparse, io, gzip, os, re, shutil, sys, tempfile, time
from collections import namedtuple as nt

etree.register_namespace("excerpt","http://wordpress.org/export/1.2/excerpt/")
etree.register_namespace("content","http://purl.org/rss/1.0/modules/content/")
etree.register_namespace("wfw","http://wellformedweb.org/CommentAPI/")
etree.register_namespace("dc","http://purl.org/dc/elements/1.1/")
etree.register_namespace("wp","http://wordpress.org/export/1.2/")

def xml_parse(input_file):
    parser = etree.XMLParser(strip_cdata=False)
    tree = etree.parse(input_file, parser)
    return tree

def strip_items_from_channel(channel):
    for item in channel.findall('item'):
        channel.remove(item)

def get_surrounding_tree(filename, working_dir):
    CACHED_SMALL_TREE = os.path.join(working_dir, "cached.xml")

    if os.access(CACHED_SMALL_TREE, os.R_OK):
        return xml_parse(CACHED_SMALL_TREE)

    else:
        surrounding_tree = xml_parse(filename)

        surrounding_channel = surrounding_tree.find('channel')
        strip_items_from_channel(surrounding_channel)
        surrounding_tree.write(CACHED_SMALL_TREE)
        return surrounding_tree

attachment = nt('attachment', ['id_', 'url', 'element'])
post = nt('post', ['id_', 'element'])

def get_id(element):
        return element.find('{http://wordpress.org/export/1.2/}post_id').text

class Posts:
    def __init__(self):
        self.by_id = {}

    def add(self, element):
        id_ = get_id(element)
        a = post(id_, element)
        self.by_id[id_] = a

class Attachments:

    def __init__(self):
        self.by_id = {}
        self.by_url = {}

    def add(self, id_, url, element):
        a = attachment(id_, url, element)
        self.by_id[id_] = a
        self.by_url[url] = a

class AttachmentsAndPosts:

    def __init__(self, attachments):
        self.attachments = attachments
        self.attachment_id_to_posts = {}
        self.post_id_to_attachments = {}

    def add(self, post_element, attachment_ids):
        post_id = get_id(post_element)
        attachments_to_post = set([])
        for id_ in attachment_ids:
            try:
                attachment = self.attachments.by_id[id_]
            except KeyError:
                print("Post id %s refers to non-existant attachment %s" % (post_id,
                        id_), file=sys.stderr)
                continue
            attachments_to_post.add(attachment)
            
            self.attachment_id_to_posts.setdefault(id_, set([])).add(
                post(post_id, post_element))
        self.post_id_to_attachments[post_id] = attachments_to_post

def get_meta_val(item, key):
    for postmeta in item.findall('{http://wordpress.org/export/1.2/}postmeta'):
        potential_key = postmeta.find('{http://wordpress.org/export/1.2/}meta_key').text
        if potential_key == key:
            return postmeta.find('{http://wordpress.org/export/1.2/}meta_value').text

def get_thumbnail_id(item, attachments):
    thumb_id = get_meta_val(item, '_thumbnail_id')
    if thumb_id is not None:
        return thumb_id

    attachment_url = get_meta_val(item, "Thumbnail")
    if attachment_url is not None:
        try:
            return attachments.by_url[attachment_url].id_
        except KeyError:
            try:
                name = item.find('{http://wordpress.org/export/1.2/}name').text
            except AttributeError:
                name = ''
            try:
                title = item.find('title').text
            except AttributeError:
                title = ''
            print("Attachment id for URL %s (referred to by post (name: %s, title: '%s') not found" % (attachment_url, name, title), file=sys.stderr)

caption_re = re.compile(r'\[caption.*?id="attachment_(\d*)"')

def get_attachments_in_body(item, attachments):
    #print (', '.join([child.tag for child in item]))
    attachment_ids = set([])
    body = item.find('{http://purl.org/rss/1.0/modules/content/}encoded').text
    if body is not None:
        for match in caption_re.finditer(body):
            attachment_ids.add(match.group(1))
        for url in attachments.by_url:
            if url in body:
                attachment_ids.add(attachments.by_url[url].id_)
    return attachment_ids

bundle = nt('bundle', ['attachments', 'posts'])

def split(filename):


    orig_tree = xml_parse(filename)

    attachments = Attachments()

    orig_channel = orig_tree.find('channel')

    parents = {}

    # parse out the attachments first
    for item in orig_channel.findall('item'):
        item_type = item.find('{http://wordpress.org/export/1.2/}post_type').text

        if item_type == 'attachment':
            item_id = get_id(item)
            url = item.find('{http://wordpress.org/export/1.2/}attachment_url').text
            attachments.add(item_id, url, item)
            parents.setdefault(item.find('{http://wordpress.org/export/1.2/}post_parent').text,
                set()).add(item_id)

    a_and_p = AttachmentsAndPosts(attachments)
    posts = Posts()

    # now look in non-attachments to find which attachments they refer to
    for item in orig_channel.findall('item'):
        item_type = item.find('{http://wordpress.org/export/1.2/}post_type').text

        if item_type != 'attachment':
            posts.add(item)
            attachment_ids = set()
            thumb_id = get_thumbnail_id(item, attachments)
            if thumb_id is not None:
                attachment_ids.add(thumb_id)
            attachment_ids = attachment_ids.union(get_attachments_in_body(item, attachments))
            attachment_ids = attachment_ids.union(parents.get(get_id(item), set()))
            a_and_p.add(item, attachment_ids)
    return attachments, posts, a_and_p

bundle = nt('bundle', ['attachments', 'posts'])

def build_bundles(attachments, posts, a_and_p):

    attachment_ids_left = set(attachments.by_id.keys())
    post_ids_left = set(a_and_p.post_id_to_attachments.keys())
    
    attachment_count = 0
    post_count = 0

    bundles = []
    while len(attachment_ids_left) > 0:
        attachment_ids_for_bundle = set()
        posts_for_bundle = set()

        # get a random remaining attachment id to begin this bundle
        bundle_attachment_queue = [attachment_ids_left.pop()]

        while len(bundle_attachment_queue) > 0:
            attachment_id = bundle_attachment_queue.pop()
            attachment_ids_for_bundle.add(attachment_id)

            posts_for_attachment = a_and_p.attachment_id_to_posts.get(attachment_id, set())
            for post_for_attachment in posts_for_attachment:
                post_id = post_for_attachment.id_
                if post_for_attachment not in posts_for_bundle:
                    post_ids_left.remove(post_id)
                    posts_for_bundle.add(post_for_attachment)
                    for new_attachment in a_and_p.post_id_to_attachments.get(post_id, set()):
                        if new_attachment.id_ not in attachment_ids_for_bundle and\
                                new_attachment.id_ not in bundle_attachment_queue:
                            bundle_attachment_queue.append(new_attachment.id_)
                            attachment_ids_left.remove(new_attachment.id_)

        attachments_for_bundle = set([attachments.by_id[id_] for id_ in attachment_ids_for_bundle])

        bundles.append(bundle(attachments_for_bundle, posts_for_bundle))

    # individual bundles for posts that have no associated attachment
    for post_id in post_ids_left:
        bundles.append(bundle(set(), set([posts.by_id[post_id]])))

    return bundles 

def bundle_size(bundle):
    return len(bundle.attachments) + len(bundle.posts)

class ExceedsSize(Exception): pass

class OutputFile:

    def __init__(self, working_dir, input_filename, output_filename, size, verbose):
        self.input_filename = input_filename
        self.output_filename = output_filename
        self.size = size
        self.working_dir = working_dir
        self.verbose = verbose
        self.verbose_counter = 0
#        print("Begin tree read")
#        start = time.time()
        self.tree = get_surrounding_tree(self.input_filename, self.working_dir)
#        print("End tree read", (time.time() - start))
        self.initial_offset = len(self.tree.find('channel'))
        self.attachment_count = 0

        self.previous_output = None

    def _add_items(self, bundle):
        channel = self.tree.find('channel')
        for offset, a in enumerate(bundle.attachments):
            channel.insert(self.initial_offset + self.attachment_count + offset, a.element)    
        self.attachment_count += len(bundle.attachments)
        for p in bundle.posts:
            channel.append(p.element)

    def add_bundle(self, bundle):
        self._add_items(bundle)
        try:
            self.store_output()
        except ExceedsSize:
            return False
        else:
            return True

    def force_add_bundle(self, bundle):
        self._add_items(bundle)
        try:
            self.store_output(force_move = True)
        except ExceedsSize:
            print("Error: file %s exceeds your maximum file size of %dMB, but cannot "
                    "be made smaller due to the interlinked posts and attachments "
                    "it contains." % (
                    self.output_filename, self.size), file=sys.stderr)
            return False
        else:
            return True

    def store_output(self, force_move = False):
        buffer_ = io.BytesIO()
        self.tree.write(buffer_)
        output = buffer_.getvalue()

        if self.verbose and self.verbose_counter % 25 == 0:
            print("Size estimate of %s now %.2fMB" % (self.output_filename, len(output) / 1000000), file=sys.stderr)
        self.verbose_counter += 1

        if len(output) / 1000000 > self.size:
            if force_move:
                self.previous_output = output
            raise ExceedsSize

        self.previous_output = output

    def write_output(self):
        of = open(self.output_filename, 'wb')
        of.write(self.previous_output)
        of.close()

class UnStrictGzipOutputFile(OutputFile):

    def store_output(self, force_move = False):
        buffer_ = io.BytesIO()
        self.tree.write(buffer_)
        output = buffer_.getvalue()

        if self.verbose and self.verbose_counter % 25 == 0:
            print("Size estimate of %s now %.2fMB" % (self.output_filename, len(output) / 4000000), file=sys.stderr)
        self.verbose_counter += 1

        if len(output) / 4000000 > self.size:
            if force_move:
                self.previous_output = output
            raise ExceedsSize

        self.previous_output = output

    def write_output(self):
        of = gzip.GzipFile(self.output_filename, 'w')
        of.write(self.previous_output)
        of.close()

class StrictGzipOutputFile(UnStrictGzipOutputFile):

    def store_output(self, force_move = False):
        buffer_ = io.BytesIO()
        self.tree.write(buffer_)
        output = buffer_.getvalue()
        zipped_output = gzip.compress(output)

        if self.verbose and self.verbose_counter % 25 == 0:
            print("Size estimate of %s now %.2fMB" % (self.output_filename, len(output) / 1000000), file=sys.stderr)
        self.verbose_counter += 1

        if len(zipped_output) / 1000000 > self.size:
            if force_move:
                self.previous_output = output
            raise ExceedsSize

        self.previous_output = output

def output(input_filename, bundles, size, gzip, strict, verbose):
    working_dir = tempfile.mkdtemp()

    bundles.sort(key = bundle_size, reverse = True)

    output_count=1
    while len(bundles) > 0:
        output_filename = 'wxr-chunk-%d.xml' % output_count
        if gzip:
            output_filename += '.gz'
            output_path = os.path.join(working_dir, output_filename)
            if strict:
                current_file = StrictGzipOutputFile(working_dir, input_filename, output_path, size, verbose)
            else:
                current_file = UnStrictGzipOutputFile(working_dir, input_filename, output_path, size, verbose)
        else:
            output_path = os.path.join(working_dir, output_filename)
            current_file = OutputFile(working_dir, input_filename, output_path, size, verbose)

        biggest_bundle = bundles.pop(0)
        
        done_biggest = not current_file.force_add_bundle(biggest_bundle)

        while not done_biggest and len(bundles) > 0:
            biggest_bundle = bundles.pop(0)
            done_biggest = not current_file.add_bundle(biggest_bundle)
            if done_biggest:
                # it didn't fit, put it back in the queue
                bundles.insert(0, biggest_bundle)

        done_smallest = False
        while not done_smallest and len(bundles) > 0:
            smallest_bundle = bundles.pop()
            done_smallest = not current_file.add_bundle(smallest_bundle)
            if done_smallest:
                # it didn't fit, put it back in the queue
                bundles.append(smallest_bundle)

        current_file.write_output()

        shutil.move(output_path, output_filename)
        print ("Upload file %s available, size %.2fMB" % (output_filename, os.stat(output_filename).st_size / 1000000))
        output_count += 1

    shutil.rmtree(working_dir)

def parse_args():
    parser = argparse.ArgumentParser(description='Split WordPress WXR file into smaller files, preserving links between attachments and posts')
    parser.add_argument('-s', '--size', metavar='SIZE', type=int, dest='size',
                   help='Maximum size of the output files in MB (default: 2)', default=2)
    parser.add_argument('--strict', dest='strict', action='store_true',
                   default=False, help='Use strict size measures rather than estimates for gzipping, decreases performance by around 4 times (default: false)')
    parser.add_argument('-z', '--gzip', dest='gzip', action='store_true',
                   default=False, help='Gzip the output files (default: false)')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                   default=False, help='Verbose output (default: false)')
    parser.add_argument('input_file', metavar='input_wxr_filename', help='Input WXR file')
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    attachments, posts, a_and_p = split(args.input_file)
    bundles = build_bundles(attachments, posts, a_and_p)
    output(args.input_file, bundles, args.size, args.gzip, args.strict, args.verbose)

if __name__ == '__main__':
    main()
